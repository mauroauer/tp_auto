#include "Arduino.h"

#define motorA1 2
#define motorA2 3
#define motorA3 4
#define motorA4 5
#define motorB1 A4
#define motorB2 A3
#define motorB3 A2
#define motorB4 A1
//#define botonEncendido 10
#define echoPin 11
#define triggerPin 12


unsigned long tiempoActual; // variable que almacena el momento en que se asigna el valor HIGH al puerto 0
bool estadoSistema;
unsigned short estadoMotor;
bool estadoSensorDistancia;
short pasoNroMotorA; //  variable que calcula el tiempo entre que el puerto 0 paso a HIGH y el puerto 1 ve ese valor
short pasoNroMotorB;
unsigned long distanciaCm;
int arrayPasos [ 4 ][ 4 ] =
{ {1, 1, 0, 0},
  {0, 1, 1, 0},
  {0, 0, 1, 1},
  {1, 0, 0, 1}
};

void serialEvent() {
	char dato = "";
	dato=Serial.read();
	switch (dato) {
		case '0':
			estadoSistema = 0;
			break;
		case '1':
			estadoSistema = 1;
				break;
		default:
			estadoSistema = 0;
			break;
	}
	//Serial.println(dato);
	//Serial.println(estadoSistema);
}


short moverMotor(short pasoNro, boolean dir, unsigned short motor) {
if (motor == 1) {
	digitalWrite(motorA1, arrayPasos[pasoNro][ 0]);
	digitalWrite(motorA2, arrayPasos[pasoNro][ 1]);
	digitalWrite(motorA3, arrayPasos[pasoNro][ 2]);
	digitalWrite(motorA4, arrayPasos[pasoNro][ 3]);
} else {
	digitalWrite(motorB1, arrayPasos[pasoNro][ 0]);
	digitalWrite(motorB2, arrayPasos[pasoNro][ 1]);
	digitalWrite(motorB3, arrayPasos[pasoNro][ 2]);
	digitalWrite(motorB4, arrayPasos[pasoNro][ 3]);
}

if (dir == true) {
    pasoNro++;
    if (pasoNro > 3) {
      pasoNro = 0;
    }
  } else {
    pasoNro--;
    if (pasoNro < 0) {
    pasoNro = 3;
    }
  }
  return pasoNro;
}

void ping() {
	long duracion;
	unsigned short tiempoEstabilizacion = 4;
	unsigned short tiempoDisparo = 10;
	unsigned long tiempoSensor = 0; //guarda el momento cuando se hace un LOW o HIGH en el puerto Trigger
	if (estadoSensorDistancia == 0){
		digitalWrite(triggerPin, LOW);
		if (tiempoActual > tiempoSensor + tiempoEstabilizacion) {
			estadoSensorDistancia = 1;
			tiempoSensor = tiempoActual;
		}
	}
	if (estadoSensorDistancia == 1){
		digitalWrite(triggerPin, HIGH);
		if (tiempoActual > tiempoSensor + tiempoDisparo) {
			digitalWrite(triggerPin, LOW);
			duracion = pulseIn(echoPin, HIGH);
			//Serial.print("duracion ");
			//Serial.println(duracion);
			//if (duracion > 90000){
			//	duracion = 9000;
			//}
			distanciaCm = duracion * 10 / 292 / 2;
			estadoSensorDistancia = 0;
			tiempoSensor = tiempoActual;
		}
	}

	//return distanciaCm;
}

void setup() {
	Serial.begin(9600);

	pinMode(motorA4, OUTPUT);
	pinMode(motorA3, OUTPUT);
	pinMode(motorA2, OUTPUT);
	pinMode(motorA1, OUTPUT);
	pinMode(motorB4, OUTPUT);
	pinMode(motorB3, OUTPUT);
	pinMode(motorB2, OUTPUT);
	pinMode(motorB1, OUTPUT);

	//pinMode(botonEncendido, INPUT);

	pinMode(triggerPin, OUTPUT);
	pinMode(echoPin, INPUT);

	tiempoActual = 0;
	estadoSistema = 0;
	estadoSensorDistancia = 0;
	estadoMotor = 1;
	pasoNroMotorA = 0;
	pasoNroMotorB = 0;
	distanciaCm = 0;
}

void loop() {
  //estadoSistema = 0; //!digitalRead(botonEncendido);
  short unsigned distanciaMinima = 10;
  int tiempoDelayMotor = 1600;
  unsigned long tiempoMotor = 0;

  // IF para determinar el debordamiento de la función millis()
  if (micros() - tiempoActual < 0) {
      tiempoActual = micros();
  } else {
      tiempoActual = micros();
      if (estadoSistema == 1) {
        if (estadoMotor == 1) {
        	if (tiempoActual > tiempoMotor + tiempoDelayMotor){
        		pasoNroMotorA = moverMotor (pasoNroMotorA, false, 1);
        		pasoNroMotorB = moverMotor (pasoNroMotorB, false, 2);
        		tiempoMotor = tiempoActual;
        	}
        	//Serial.println(pasoNroMotorA);
        	//Serial.println(pasoNroMotorB);
        	Serial.println("estadoMotor=1");
        	Serial.println(tiempoActual);
        	Serial.println(tiempoMotor);
        	ping();
        	Serial.println(distanciaCm);
        	if (distanciaCm < distanciaMinima) {
        		estadoMotor = 2;
        	}
        }
        if (estadoMotor == 2 ) {
        	if (tiempoActual > tiempoMotor + tiempoDelayMotor){
        		pasoNroMotorA = moverMotor (pasoNroMotorA, false, 1);
        		pasoNroMotorB = moverMotor (pasoNroMotorB, true, 2);
        	}
        	ping();
        	Serial.println(distanciaCm);
        	Serial.println("estadoMotor=2");
        	if (distanciaCm > distanciaMinima) {
        		estadoMotor = 1;
        	}
        }
      } else {
    	  estadoSistema = 0; //digitalWrite(botonEncendido, LOW);
      }
  }
}
